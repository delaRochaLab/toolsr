#LEARNING_RATE = 0.1
COUNTOUR = 500

# Size of kernel for morphology opening and closing
SIZE_CLOSING = 5
SIZE_OPENING = 3

# Parameter for MOG2
MOG_HISTORY = 500
MOG_THRESHOLD = 16

# Threshold to detect the rat inside the box
THRESH_POINTS_RATE = 0.4

THRESH_INTENCITY_FRAME = 200