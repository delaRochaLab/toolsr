# !/usr/bin/python3
# -*- coding: utf-8 -*-

import cv2
from toolsR.VideoTracking.algorithm import Algorithm

"""
Class useful to test the tracking. 
"""


class TrackingR:

    def __init__(self, indx_or_path = 0, nAreas = 3, address = None):
        self.video = cv2.VideoCapture(indx_or_path)
        self.alg = Algorithm(nAreas, address)

    def start(self):
        """ Start tracking """

        self.video.set(1, 8000) # start video from 20 sec after

        while True:
            ok, frame = self.video.read()
            if not ok:
                print('Error 001: no more frame.')
                break

            mask, frame = self.alg.apply(frame)
            cv2.imshow("maskMOG2", mask)

            # show the frame and record if the user presses a key
            cv2.imshow("Streaming", frame)
            key = cv2.waitKey(1) & 0xFF

            # if the `q` key is pressed, break from the lop
            if key == ord("q"):
                break

        # Close
        self.__close(self.video)

    def __close(self, video):
        """ cleanup the camera and close any open windows """
        video.release()
        cv2.destroyAllWindows()
