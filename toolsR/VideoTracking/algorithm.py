# !/usr/bin/python3
# -*- coding: utf-8 -*-

import cv2
import numpy as np
from toolsR.VideoTracking.settings import *
from toolsR.utils.SoftcodeConnection import SoftcodeConnection

class Algorithm:

    def __init__(self, nAreas = 3, address = None):
        self._fgbgMOG2 = cv2.createBackgroundSubtractorMOG2(MOG_HISTORY, MOG_THRESHOLD, False)

        self._kernel_open = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (SIZE_OPENING, SIZE_OPENING))
        self._kernel_close = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (SIZE_CLOSING, SIZE_CLOSING))

        # Boxer list
        self._nAreas = nAreas
        self._boxes = None
        self._flag_boxes = []

        # Softcode connection
        self._connection = None
        if address is not None:
            if type(address) is not int:
                raise ValueError(
                    'The address has to be integer. To comunicate to bpod check the address before use this method.')
            self._connection = SoftcodeConnection(address)

    def apply(self, frame):
        """ Apply the algorithm """
        self.__selectBoxes(frame)

        gray = self.__preProcessing(frame)
        mask = self._fgbgMOG2.apply(gray)
        mask = self.__makeCompact(mask)

        idx = None
        if np.mean(mask) < THRESH_INTENCITY_FRAME: # Filter the frame with high intencity of changes
            # Check the boxes
            idx = self.__checkTr(mask)
            if idx is not None and not self._flag_boxes[idx]:
                self._flag_boxes[idx] = True # to avoid all the calls in the same moment
                #send soft code
                if self._connection is not None:
                    self._connection.send(idx + 1) # The index is changed for the Softcodes
            elif idx is None:
                self._flag_boxes = [False]*len(self._flag_boxes) # Reset 

        return mask, self._drawAreas(frame, idx)

    def __preProcessing(self, frame):
        """ resize the frame, convert it to grayscale, and blur it """
        # frame = imutils.resize(frame, width=500)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)
        return gray

    def __makeCompact(self, mask):
        """Opening and closing morphology """

        # - Opening
        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, self._kernel_open)
        # mask = self._convexHull(mask)

        # - Closing
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, self._kernel_close)

        # mask = cv2.erode(mask, self.kernel_open2, iterations=1)
        # mask = cv2.dilate(mask, self.kernel_close2, iterations=1)
        return mask

    def __selectBoxes(self, frame):
        """ Select by mouse the areas """
        if self._boxes is None:
            self._boxes = []
            for i in range(self._nAreas):
                self._boxes.append(cv2.selectROI(frame))
                cv2.destroyAllWindows()
                frame = self._drawAreas(frame)
                self._flag_boxes.append(False)

    def _drawAreas(self, frame, idx = None):
        """ Draw rectangles """
        count = 0
        for a in self._boxes:
            x = int(a[0])
            y = int(a[1])
            w = x + int(a[2])
            h = y + int(a[3])
            if idx is not None and idx == count:
                frame = cv2.rectangle(frame, (x, y), (w, h), (0, 255, 0), 2) # green rec
            else:
                frame = cv2.rectangle(frame, (x, y), (w, h), (0, 0, 255), 2) # red rec
            count += 1

        return frame

    def __checkTr(self, mask):
        """ Retutn the box index where there are more foreground points """
        count_point = []
        for a in self._boxes:
            x = int(a[0])
            y = int(a[1])
            w = x + int(a[2])
            h = y + int(a[3])

            crop_box = mask[y:h,x:w]
            nonzero = np.count_nonzero(crop_box)

            if nonzero > a[2]*a[3]*THRESH_POINTS_RATE: count_point.append(nonzero)
            else: count_point.append(0)

        max_box = count_point.index(max(count_point))
        if count_point[max_box] == 0:
            return None
        else:
            return max_box

    def close(self):
        self._boxes = None
        self.connection.close()