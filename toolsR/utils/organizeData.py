import json, numpy as np
from time import gmtime, strftime


class DataR:
    def __init__(self, path=None, name=None, yourDATA=None):
        """ Prepare the structure """
        self.__mydata = {}
        self.__mydata['State'] = {}
        self.__mydata['Event'] = {}
        self.__mydata['bpod_timestamp'] = []
        self.__mydata['Info'] = yourDATA  # <--- It will save None if you miss that value
        self.__mydata['Correct'] = []

        if path is not None:  # Set the path where to save the file, as default it will be saved where you are running the program
            self.PATH = path
        else:
            self.PATH = ''

        if name is not None:  # Set the name where to save the path, default = timer
            self.NAME = name
        else:
            self.NAME = self.__getTime()   # write the name according to the information that you want on the file


            # write the name according to the information that you want on the file

    def add(self, newdata, correctside=None):
        """ Add new row to the data"""
        # Get all the info and separete them
        bpod_start_timestamp = newdata['Bpod start timestamp']
        print(bpod_start_timestamp)
        timestamps_by_state = newdata['States timestamps']
        print(timestamps_by_state)
        timestamps_by_event = newdata['Events timestamps']
        print(timestamps_by_event)
        if correctside is not None:
            self.__mydata['Correct'].append(correctside)

        # Save the states timestamps
        for name_state, value_state in timestamps_by_state.items():
            if not name_state in self.__mydata['State']:  # Create it if not exist yet
                self.__mydata['State'][name_state] = []
            self.__mydata['State'][name_state].append(value_state)

        # Save the events timestamps
        for name_event, value_event in timestamps_by_event.items():  # Create it if not exist yet
            if not name_event in self.__mydata['Event']:
                self.__mydata['Event'][name_event] = []
            self.__mydata['Event'][name_event].append(value_event)

        if not 'Port1Out' in timestamps_by_event:
            if not 'Port1Out' in self.__mydata['Event']:
                self.__mydata['Event']['Port1Out'] = []
            self.__mydata['Event']['Port1Out'].append([np.NaN])

        if not 'Port1In' in timestamps_by_event:
            if not 'Port1In' in self.__mydata['Event']:
                self.__mydata['Event']['Port1In'] = []
            self.__mydata['Event']['Port1In'].append([np.NaN])

        if not 'RotaryEncoder1_1' in timestamps_by_event:
            if not 'RotaryEncoder1_1' in self.__mydata['Event']:
                self.__mydata['Event']['RotaryEncoder1_1'] = []
            self.__mydata['Event']['RotaryEncoder1_1'].append([np.NaN])

        if not 'RotaryEncoder1_2' in timestamps_by_event:
            if not 'RotaryEncoder1_2' in self.__mydata['Event']:
                self.__mydata['Event']['RotaryEncoder1_2'] = []
            self.__mydata['Event']['RotaryEncoder1_2'].append([np.NaN])

            # Save the timestamp of bpod
        self.__mydata['bpod_timestamp'].append(bpod_start_timestamp)

    '''
    def savePKL(self):
        """ Write the data in a file. """
        output = open(self.PATH + self.NAME + '.pkl', 'wb')
        pickle.dump(self.__mydata, output)
        output.close()

    def openPKL(self):
        pkl_file = open('myfile.pkl', 'rb')
        self.__mydata = pickle.load(pkl_file)
        pkl_file.close()

    def saveCSV(self):
        w = csv.writer(open(self.PATH + self.NAME + ".csv", "w"))
        for key, val in self.__mydata.items():
            w.writerow([key, val])
    '''

    def save(self):
        with open(self.PATH + self.NAME + '.json', 'w') as fp:
            json.dump(self.__mydata, fp)

    def open(self, name):
        """ Open json file with the structure. """
        with open(name + '.json', 'r') as fp:
            self.__mydata = json.load(fp)
        return self.__mydata

    def getStates(self):
        """ Return the vector State """
        return self.__mydata['State']

    def getEvents(self):
        """ Return the vector Event """
        return self.__mydata['Event']

    def getBpodTimestamps(self):
        """ Return the vector bpod_timestamp """
        return self.__mydata['bpod_timestamp']

    def __getTime(self):
        """ Get the time, useful for the name """
        return strftime("%d%b%Y_%H%M%S", gmtime())

    def getTrials(self):
        return self.__mydata['Correct']

    def getInfo(self):
        return self.__mydata['Info']