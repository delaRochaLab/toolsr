# !/usr/bin/python3
# -*- coding: utf-8 -*-

import timeit

class TimerR:
    def __init__(self):
        self._timeS = 0.
        self._current_time = 0.
        self._isRunning = False

    def start(self):
        self._timeS = timeit.default_timer()
        self._isRunning = True

    def pause(self):
        last_time = self._getTimeRaw()
        if last_time == -1:
            return
        else:
            self._current_time = self._current_time + self._getTimeRaw()
            self._timeS = 0.

    def stop(self):
        self._isRunning = False

    def reset(self):
        self._timeS = 0.
        self._current_time = 0.

    def isRunning(self):
        return self._isRunning

    def isPause(self):
        if self._timeS == 0 and self.isRunning():
            return True
        else:
            return False

    def getTimeSeconds(self):
        last_time = self._getTimeRaw()
        if last_time == -1:
            return self._current_time
        else:
            return self._current_time + last_time

    def getString(self):
        t = self.getTimeSeconds()
        h = int(t/3600)
        m = int((t - h*3600)/60)
        s = int(t - h*3600 - m*60)
        return '{}:{}:{}'.format(h, m, s)

    def _getTimeRaw(self):
        if not self.isRunning():
            print('TimeR: You should use start() before.')
            return -1
        if self._timeS == 0:
            return -1
        else:
            return timeit.default_timer() - self._timeS

    def _getTimeSeconds(self):
        return self._getTimeRaw() - self.timePause