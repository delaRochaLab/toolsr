import numpy as np
import os
import csv
import subprocess
from scipy.signal import firwin, lfilter  # filters
from tkinter import Tk, Frame, Button, StringVar, OptionMenu, Label, N, W, E, S, messagebox, simpledialog, Checkbutton, IntVar
from scipy.integrate import cumtrapz
from functools import partial
from timeit import default_timer as timer
import hashlib
from shutil import copyfile

if 'SLACK_BOT_TOKEN' in os.environ:
    import slack

# COMPUTE TIME
class Timing:
    """
    Print the time since the previous time() call. First time it prints zero.
    """

    def __init__(self):
        t = timer()
        self.times = [t, t]
        print('starting timing')

    def time(self, printing=True, text=''):
        t = timer()
        self.times[1] = t - self.times[0]
        self.times[0] = t

        if printing:
            print(text, ':   ', str(self.times[1]), 'seconds  /   ', str(self.times[1] * 1000), 'miliseconds')



def stop(message=''):
    if message != '':
        messagebox.showinfo('INFO', message)
    raise SystemExit(0)


def show(message):
    messagebox.showinfo('INFO', message)


def get_git_revision_hash(dirpath, taskname):
    """now returns extra stuff"""
    session_dir = os.getcwd()
    os.chdir(os.path.expanduser(dirpath))
    commithash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    os.chdir(session_dir)
    digester = hashlib.md5()
    path = f'{os.path.expanduser(dirpath)}/{taskname}/{taskname}.py'
    with open(path, 'rb') as f:
        # straight outta SO: https://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file
        for chunk in iter(lambda: f.read(4096), b""):
            digester.update(chunk)
    checksum = digester.hexdigest()
    backup_dir = os.path.expanduser('~/backup-tasks')
    if not os.path.exists(backup_dir):
        os.mkdir(backup_dir)
    new_path = f'{os.path.expanduser(backup_dir)}/{taskname}_{checksum}.py'
    if not os.path.exists(new_path):
        copyfile(path, new_path)
    return commithash, checksum


def getWaterCalib(board, ports):
    log_name = os.path.expanduser("~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration.csv")
    with open(log_name, 'r') as log:
        calibration_data = csv.DictReader(log, delimiter=';')
        latest_row = None
        for row in calibration_data:
            if row['board'] == board:
                latest_row = dict(row)
        if latest_row is None:
            raise Exception("Water calibration data not found.")
        else:
            results = latest_row['pulse_duration'].split("-")
            return [float(results[i-1]) for i in ports]


def getBroadbandCalib(board):
    """
    returns tupple (L,R) of amp [latter calib values] of a given soundcard index (same as SoundR.getDevices())
    to-do: add second argument, dB or range of freq to look for.
    """
    broadband = np.load(
        os.path.expanduser('~/pluginsr-for-pybpod/sound-calibration-plugin/DATA/broadbandnoise_hystory.npy'),
        allow_pickle=True).item()

    left_idx = next(i for i, v, w in
                    zip(range(len(list(broadband['Bsoundcard'])) - 1, -1, -1), reversed(list(broadband['Bsoundcard'])),
                        reversed(list(broadband['Bside']))) if v.startswith(board) and w == 'L')
    right_idx = next(i for i, v, w in
                     zip(range(len(list(broadband['Bsoundcard'])) - 1, -1, -1), reversed(list(broadband['Bsoundcard'])),
                         reversed(list(broadband['Bside']))) if v.startswith(board) and w == 'R')

    return broadband['BAmp'][left_idx], broadband['BAmp'][right_idx]


def getFrequencyCalib(board, freq1, freq2):

    data = np.load(
        os.path.expanduser('~/pluginsr-for-pybpod/sound-calibration-plugin/DATA/puretone_hystory.npy'),
        allow_pickle=True).item()

    freq1_left = next(i for i, v, w, x in
                    zip(range(len(list(data['Psoundcard'])) - 1, -1, -1), reversed(list(data['Psoundcard'])),
                        reversed(list(data['Pside'])),reversed(list(data['PFreq'])))
                    if v.startswith(board) and w == 'L' and abs(freq1 - x) <= 0.1)

    freq1_right = next(i for i, v, w, x in
                    zip(range(len(list(data['Psoundcard'])) - 1, -1, -1), reversed(list(data['Psoundcard'])),
                        reversed(list(data['Pside'])),reversed(list(data['PFreq'])))
                    if v.startswith(board) and w == 'R' and abs(freq1 - x) <= 0.1)

    freq2_left = next(i for i, v, w, x in
                      zip(range(len(list(data['Psoundcard'])) - 1, -1, -1), reversed(list(data['Psoundcard'])),
                          reversed(list(data['Pside'])), reversed(list(data['PFreq'])))
                      if v.startswith(board) and w == 'L' and abs(freq2 - x) <= 0.1)

    freq2_right = next(i for i, v, w, x in
                       zip(range(len(list(data['Psoundcard'])) - 1, -1, -1), reversed(list(data['Psoundcard'])),
                           reversed(list(data['Pside'])), reversed(list(data['PFreq'])))
                       if v.startswith(board) and w == 'R' and abs(freq2 - x) <= 0.1)

    return data['PAmp'][freq1_left], data['PAmp'][freq1_right], data['PAmp'][freq2_left], data['PAmp'][freq2_right]


def whiteNoiseGen(amp, band_fs_bot, band_fs_top, duration, FsOut=192000, Fn=10000, randgen=None):
    """whiteNoiseGen(amp, band_fs_bot, band_fs_top):
    beware this is not actually whitenoise
    amp: float, amplitude
    band_fs_bot: int, bottom freq of the band
    band_fs_top: int, top freq
    duration: secs
    FsOut: SoundCard samplingrate to use (192k, 96k, 48k...)
    Fn: filter len, def 10k
    *** if this takes too long try shortening Fn or using a lower FsOut ***
    adding some values here. Not meant to change them usually.
    randgen: np.random.RandomState instance to sample from

    returns: sound vector (np.array)
    """
    mean = 0
    std = 1
    if randgen is None:
        randgen = np.random

    if type(amp) is float and isinstance(band_fs_top, int) and isinstance(band_fs_bot,
                                                                          int) and band_fs_bot < band_fs_top:
        band_fs = [band_fs_bot, band_fs_top]
        white_noise = amp * randgen.normal(mean, std, size=int(FsOut * (duration + 1)))
        band_pass = firwin(Fn, [band_fs[0] / (FsOut * 0.5), band_fs[1] / (FsOut * 0.5)], pass_zero=False)
        band_noise = lfilter(band_pass, 1, white_noise)
        s1 = band_noise[FsOut:int(FsOut * (duration + 1))]
        return s1  # use np.zeros(s1.size) to get equal-size empty vec.
    else:
        raise ValueError('whiteNoiseGen needs (float, int, int, num,) as arguments')


def pureToneGen(amp, freq, toneDuration, FsOut=192000):
    """generates a given parameters pure tone vector. Gen counterpart using np.empty(s1.shape[?])
    pureToneGen(amp, freq, toneDuration, FsOut=192000):
    """
    if type(amp) is float and type(freq) is int:
        tvec = np.linspace(0, toneDuration, toneDuration * FsOut)
        s1 = amp * np.sin(2 * np.pi * freq * tvec)
        return s1
    else:
        raise ValueError('pureToneGen needs (float, int) as arguments')


def envelope(coh, whitenoise, dur, nframes, samplingR=192000, variance=0.015, randomized=False, paired=True, LAmp=1.0,
             RAmp=1.0, oldbug=True, randgen=None):
    """
    coh: coherence from 0(left only)to 1(right). ! var < coh < (1-var). Else this wont work
    whitenoise: vec containing sound (not necessarily whitenoise)
    dur: total duration of the stimulus (secs)
    nframes: total frames in the whole stimulus
    samplingR: soundcard sampling rate (ie 96000). Need to match with EVERYTHING
    variance: fixed var
    randomized: shuffles noise vec
    paired: each instantaneous evidence is paired with its counterpart so their sum = 1
    randgen: np.random.RandomState instance to sample from
    returns: left noise vec, right noise vec, left coh stairs, right coh stairs [being them all 1d-arrays]
    """
    if randgen is None:
        randgen = np.random
    totpoints = dur * samplingR  # should be an integer
    if len(whitenoise) < totpoints:
        raise ValueError('whitenoise is shorter than expected')

    if randomized == True:
        svec = whitenoise[:int(totpoints)]
        svec = svec.reshape(int(len(svec) / 10), 10)
        randgen.shuffle(svec)
        svec = svec.flatten()
    else:
        svec = whitenoise[:int(totpoints)]
    modfreq = nframes / dur
    if oldbug: # when freq was doubled, maintaining it because of compatibility issues. #envs = #stairs*2
        modwave = 1 * np.sin(2 * np.pi * (modfreq) * np.arange(0, dur, step=1 / samplingR) + np.pi)
    else: # bug fixed, stairs paired with envelopes (#envs = #stairs)
        modwave = 0.5 * (np.sin(2 * np.pi * (modfreq) * np.arange(0, dur, step=1 / samplingR) - np.pi/2)+1)

    if coh < 0 or coh > 1:
        raise ValueError(f'{coh} is an invalid coherence, it must fall w/i range 0 ~ 1')

    elif coh == 0 or coh == 1:
        staircaseR = np.repeat(coh, dur * samplingR)
        staircaseL = staircaseR - 1
        Lout = staircaseL * svec * modwave * LAmp
        Rout = staircaseR * svec * modwave * RAmp
        return Lout, Rout, np.repeat(coh - 1, nframes), np.repeat(coh, nframes)
    elif coh <= (variance * 1.1) or coh >= 1 - variance * 1.1:
        raise ValueError('invalid coherence for given variance or viceversa (if coh!=0|1, 1.1*var<coh<1-var*1.1)')
    else:
        alpha = ((1 - coh) / variance - 1 / coh) * coh ** 2
        beta = alpha * (1 / coh - 1)
        stairs_envelopeR = randgen.beta(alpha, beta, size=nframes)
        staircaseR = np.repeat(stairs_envelopeR, int(totpoints / nframes))
        staircaseL = staircaseR - 1
        Rout = staircaseR * svec * modwave * RAmp
        if paired == False:
            stairs_envelopeL = randgen.beta(alpha, beta, size=nframes) - 1
            staircaseL = np.repeat(stairs_envelopeL, int(totpoints / nframes))
            Lout = staircaseL * svec * modwave * LAmp
            return Lout, Rout, stairs_envelopeL, stairs_envelopeR
        Lout = staircaseL * svec * modwave * LAmp
        return Lout, Rout, stairs_envelopeR - 1, stairs_envelopeR


def generate_sound_freq(stim_evidence, sound_duration, modulator_freq, variance, sound1_freq, sound2_freq,
                        sound1_left_amp, sound1_right_amp, sound2_left_amp, sound2_right_amp, out_freq, randgen=None):

    if randgen is None:
        randgen = np.random

    if out_freq % modulator_freq != 0:
        raise ValueError('out_freq must be multiple of modulator_freq')

    number_of_samples = int(sound_duration * out_freq)
    number_of_frames = int(sound_duration * modulator_freq)
    samples_per_frame = int(number_of_samples / number_of_frames)

    if stim_evidence in [0, 1]:
        instantaneous_evidence_frame = np.repeat(stim_evidence, number_of_frames)
        instantaneous_evidence_sample = np.repeat(stim_evidence, number_of_samples)

    else:
        alpha = ((1 - stim_evidence) / variance - 1 / stim_evidence) * stim_evidence ** 2

        beta = alpha * (1 / stim_evidence - 1)

        instantaneous_evidence_frame = randgen.beta(alpha, beta, size=number_of_frames)
        instantaneous_evidence_sample = np.repeat(instantaneous_evidence_frame, samples_per_frame)

    sound1_amplitude = 1 - instantaneous_evidence_sample
    sound2_amplitude = instantaneous_evidence_sample

    tvec = np.linspace(0, sound_duration, number_of_samples)

    sound1 = np.sin(2 * np.pi * sound1_freq * tvec)
    sound2 = np.sin(2 * np.pi * sound2_freq * tvec)

    mod = (1 + np.sin(2 * np.pi * modulator_freq * tvec + 3 * np.pi / 2)) / 2

    stim_left = mod * (sound1_left_amp * sound1_amplitude * sound1 + sound2_left_amp * sound2_amplitude * sound2)
    stim_right = mod * (sound1_right_amp * sound1_amplitude * sound1 + sound2_right_amp * sound2_amplitude * sound2)

    return stim_left, stim_right, 1 - instantaneous_evidence_frame, instantaneous_evidence_frame


def block(ss, repetitive=True, bsize=200, bnum=10, randgen=None):
    """
    ss: int, starting side; 0=left; 1=right
    repetitive: bool, repetitve or alt block (prob 80/20 but swapped)
    bsize: blocksize (int)
    bnum: number of blocks to output (alternating from repetitive to alternating)
    randgen: np.random.RandomState instance to sample from
    returns
        out: 1-d array of 0 and 1
        prob_repeat: 1d array of prob_repeat
    """
    if randgen is None:
        randgen=np.random
    # some security checks
    if ss not in [0, 1]:
        raise ValueError('ss: starting side, int, either 0 (left) or 1(right)')
    if isinstance(repetitive, bool) != True:
        raise ValueError('repetitive: bool, True->repetitive block; False-> alternating block')
    if isinstance(bsize, int) != True:
        raise ValueError('bsize: int, blocksize')
    if not (isinstance(bnum, int) and bnum >= 1 == True):
        raise ValueError('bnum: int <= 1')
    out = [ss]
    prob_repeat = []
    for _ in range(bnum):
        if repetitive == True:
            prob = [0.8, 0.2]
            prob_repeat = prob_repeat + [prob[0]] * bsize
        else:
            prob = [0.2, 0.8]
            prob_repeat = prob_repeat + [prob[0]] * bsize
        rep_alt_vec = randgen.choice([0, 1], bsize, replace=True, p=prob)  # being 0 repeat and 1 alt
        # generate L/R vec from starting point
        for i in range(len(rep_alt_vec)):
            out.append(abs(out[-1] - rep_alt_vec[i]))
        repetitive = not repetitive  # once finished block, next one will be a different one
    return out, prob_repeat


def block_harder(ss, repetitive=True, bsize=200, bnum=10, randgen=None):
    """
    ss: int, starting side; 0=left; 1=right
    repetitive: bool, repetitve or alt block (prob 80/20 but swapped)
    bsize: blocksize (int)
    bnum: number of blocks to output (alternating from repetitive to alternating)
    randgen: np.random.RandomState instance to sample from
    returns
        out: 1-d array of 0 and 1
        prob_repeat: 1d array of prob_repeat
    """
    if randgen is None:
        randgen=np.random
    # some security checks
    if ss not in [0, 1]:
        raise ValueError('ss: starting side, int, either 0 (left) or 1(right)')
    if isinstance(repetitive, bool) != True:
        raise ValueError('repetitive: bool, True->repetitive block; False-> alternating block')
    if isinstance(bsize, int) != True:
        raise ValueError('bsize: int, blocksize')
    if not (isinstance(bnum, int) and bnum >= 1 == True):
        raise ValueError('bnum: int <= 1')
    out = [ss]
    prob_repeat = []
    for _ in range(bnum):
        if repetitive == True:
            prob = [0.7, 0.3]
            prob_repeat = prob_repeat + [prob[0]] * bsize
        else:
            prob = [0.2, 0.8]
            prob_repeat = prob_repeat + [prob[0]] * bsize
        rep_alt_vec = randgen.choice([0, 1], bsize, replace=True, p=prob)  # being 0 repeat and 1 alt
        # generate L/R vec from starting point
        for i in range(len(rep_alt_vec)):
            out.append(abs(out[-1] - rep_alt_vec[i]))
        repetitive = not repetitive  # once finished block, next one will be a different one
    return out, prob_repeat



# legacy and semantically incorrect stuff
def evidencetocoh(x):
    return (x + 1) / 2


def cohtoevidence(x):
    return 2 * x - 1


def select_evidence(trialtype, ev_list, randgen=None):  # debug
    """
    for the sake of reducing 0-coh prob(1/2) and ease of use
    trialtype: int, 0=left, 1=right
    ev_list: array, contains all posible evidences.
    randgen: np.random.RandomState instance to sample from
    returns: COHERENCE from 0 (left) to 1(right). ie. 0 net evidence returns 0.5. Why?
    So we can calc beta distr directly. See envelope funct.
    """
    if randgen is None:
        randgen = np.random
    ev_list = np.array(ev_list)
    if trialtype == 0:
        available = ev_list[ev_list <= 0]  # evidences ccorresponding to Left
    else:
        available = ev_list[ev_list >= 0]  # evidences corresponding to Right trials
    if len(np.unique(ev_list == 0)) == 1:  # All falses no 0, all fine
        selected_evidence = randgen.choice(available)
    else:  # 0 evidence, where?
        zeroloc = np.where(available == 0)[0][
            0]  # index of item==0 in our vector available (available evidences for that particular reward side)
        currprob = 1 / len(ev_list)
        prob_vec = np.repeat(currprob * 2, len(available))
        prob_vec[
            zeroloc] = currprob  # 1/2 prob of evidence=0 (so if you add both rew sides ev=0 ;
        # ev=0 appears  with the same prob thhat other particular ev (eg -1 and 1))
        selected_evidence = randgen.choice(available, p=prob_vec)

    return (selected_evidence + 1) / 2


def antibias(bumpside, window, reward_side_vec, bump_prob, curr_trial_idx, randgen=None):
    """
    bumpside: int ([0,1]), left or right, side to incr probability
    window: int >=1, window len of trials to apply antibias
    reward_side_vec: list, containing 0 and 1s (0, left; 1, right)
    bump_prob: float, from 0 to 1. prob that trials != bumpside within window shift.
    randgen: np.random.RandomState instance to sample from
    returns: (whole) reward_side_vec with updated window
    """
    if randgen is None:
        randgen=np.random
    trialWin = np.array(reward_side_vec[curr_trial_idx + 1:curr_trial_idx + window + 1])
    loc = np.where(trialWin != bumpside)[0]  # getting location of trials to switch
    if bumpside == 0:
        replacement = [1, 0]  # ones likely to be replaced with 0s' with bump_prob
    elif bumpside == 1:
        replacement = [0, 1]  # 0s likely to be replaced with 1s' with bump prob.
    alt_vec = randgen.choice(replacement, len(loc), replace=True, p=[1 - bump_prob, bump_prob])  # replacement vector
    trialWin[loc] = alt_vec
    reward_side_vec[curr_trial_idx + 1:curr_trial_idx + window + 1] = trialWin.tolist()
    return reward_side_vec


def dropdownmenu(titlestr, labelstr, options_dic, default_opt):
    """
    since it was easy to forget updating variations for logging purposes,
    this function helps generating a dropdown popup
    titlestr: String, window title
    labelstr: String, label
    options_dic: akward dict (just comma delimited keys,) containing dropdown menu options
    default_opt: default option from the above dict keys
    """
    root = Tk()
    root.eval('tk::PlaceWindow . center')
    root.title(titlestr)

    def show_entry_fields():
        global dummy
        dummy = tkvar.get()
        root.destroy()

    # Add a grid
    mainframe = Frame(root)
    mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
    mainframe.columnconfigure(0, weight=1)
    mainframe.rowconfigure(0, weight=1)
    mainframe.pack(pady=100, padx=50)
    Button(mainframe, text='Quit', command=mainframe.quit).grid(row=3, column=0, sticky=W, pady=4)
    Button(mainframe, text='Confirm', command=show_entry_fields).grid(row=3, column=1, sticky=W, pady=4)

    # Create a Tkinter variable
    tkvar = StringVar(root)

    # Dictionary with options
    dropdownchoices = options_dic
    tkvar.set(default_opt)  # set the default option

    popup_menu = OptionMenu(mainframe, tkvar, *dropdownchoices)
    Label(mainframe, text=labelstr).grid(row=1, column=1)
    popup_menu.grid(row=2, column=1)

    # on change dropdown value
    def change_dropdown(*args):
        print(tkvar.get())

    # link function to change dropdown
    tkvar.trace('w', change_dropdown)

    root.mainloop()

    return dummy

def simplemenu(titlestr, options_dic):

    root = Tk()
    root.eval('tk::PlaceWindow . center')
    root.title(titlestr)

    def action(value):
        global dummy
        dummy = value
        root.destroy()

    # Add a grid
    mainframe = Frame(root)
    mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
    mainframe.columnconfigure(0, weight=1)
    mainframe.rowconfigure(0, weight=1)
    mainframe.pack(pady=100, padx=50)

    i = 3
    for value in options_dic:
        Button(mainframe, text=value, command=partial(action, value)).grid(row=i, column=0, sticky=W, pady=4)
        i += 1

    root.mainloop()

    return dummy


def inputmenu(targettitle, question):
    suppl = Tk()
    suppl.eval('tk::PlaceWindow . center')
    suppl.title(targettitle)
    response = simpledialog.askstring(question, suppl)
    suppl.destroy()
    return response

def checkbuttonmenu(vardict, title='choose', label='choose among options'):
    """"
    vardict: dict containing var_names as keys and default value as item
    returns: dict with updated choices
    """
    klist = list(vardict.keys())
    root=Tk()
    tmp_dict = {k:IntVar(value=vardict[k]) for k in klist}
    commonkwargs = dict(onvalue=1, offvalue=0, height=5, width=20)
    Label(root, text=label).grid(row=0, sticky=W)
    for i, k in enumerate(klist):
        Checkbutton(root, text=k, variable=tmp_dict[k], **commonkwargs).grid(row=i+1, sticky=W)
    Button(root, text='Confirm', command=root.quit, height=5, width=10).grid(row=len(klist)+1, sticky=S)
    root.title('settings')
    root.eval('tk::PlaceWindow . center')
    root.mainloop()
    out = {k:tmp_dict[k].get() for k in klist}
    root.destroy()
    return out

def block_side_enhanced(ss, rightenhanced=True, bsize=200, bnum=10, randomblock=False, randgen=None):
    """
    ss: int, starting side; 0=left; 1=right
    rightenhanced: bool, right enhanced (True, 80%) or else (False --> left enhanced, 80%)
    bsize: blocksize (int)
    bnum: number of blocks to output (alternating from right enhanced to left enhanced side)
    randgen: np.random.RandomState instance to sample from
    returns:
        right_left vec: 1-d array of 0 and 1 (left/right)
        side_enhanced: returns a list of 'R' 'L' depending in blocktype
        TODO: is this the intended behavior? it would be kewl to retrieve actual p(repeat)
            rather than the side with higher probability. @Dani @Rafa
    """
    # some security checks
    if ss not in [0, 1]:
        raise ValueError('ss: starting side, int, either 0 (left) or 1(right)')
    if isinstance(rightenhanced, bool) != True:
        raise ValueError('rightenhanced: bool, True-> right enhanced; False-> left enhanced')
    if isinstance(bsize, int) != True:
        raise ValueError('bsize: int, blocksize')
    if not (isinstance(bnum, int) and bnum >= 1 == True):
        raise ValueError('bnum: int <= 1')
    if randgen is None:
        randgen = np.random
    # Initialize output vectors
    right_left_vec = [ss]
    if ss == 0:
        side_enhanced = ['L']
    else:
        side_enhanced = ['R']

    # Generates the blocks and label them accordingly (R or L)
    for _ in range(bnum):
        if rightenhanced == True:
            if len(right_left_vec) == 1:
                side_enhanced.extend('R' * (bsize - 1))
            else:
                side_enhanced.extend('R' * (bsize))
        elif rightenhanced == False:
            if len(right_left_vec) == 1:
                side_enhanced.extend('L' * (bsize - 1))
            else:
                side_enhanced.extend('L' * (bsize))

        # generate L/R vector
        for _ in range(1, bsize):
            # If R enhanced and previous was R
            if rightenhanced == True and right_left_vec[-1] == 1:
                current_trial = randgen.choice([0, 1], 1, replace=True, p=[0.24, 0.76])
                right_left_vec.append(current_trial[0])
            # If R enhanced and previous was L
            elif rightenhanced == True and right_left_vec[-1] == 0:
                current_trial = randgen.choice([0, 1], 1, replace=True, p=[0.04, 0.96])
                right_left_vec.append(current_trial[0])
            # If L enhanced and previous was L
            elif rightenhanced == False and right_left_vec[-1] == 0:
                current_trial = randgen.choice([0, 1], 1, replace=True, p=[0.76, 0.24])
                right_left_vec.append(current_trial[0])
            # If L enhanced and previous was R
            elif rightenhanced == False and right_left_vec[-1] == 1:
                current_trial = randgen.choice([0, 1], 1, replace=True, p=[0.96, 0.04])
                right_left_vec.append(current_trial[0])

        # once finished block, next one will be a different one (do we really want that?)
        if randomblock:
            rightenhanced = bool(randgen.choice([0, 1]))
        else:
            rightenhanced = not rightenhanced

    return right_left_vec, side_enhanced


def newBlock(tm, ss='None', sblock='None', bnum=26, blen=80, block_seq='None', block_lens='None', randgen=None):
    """
    if any block_seq and are provided, this function should ignore bnum and/or blen, huehue future to do
    tm: transition matrix 2d array; rows being block type, and cols p(repeat when L, repeat when R),
    complementary p(alt) will be 1-p(rep) since its 2afc
    ss: starting side (0=L, 1=R)
    sblock= starting block (row index of corresponding probs in tm)
    bnum = total blocks
    blen = trial length of each block
    block_seq= list of blocktypes
    block_lens= list of lengths (1 item = 1 block)
    randgen: np.random.RandomState instance to sample from

    returns:
        out: vector of 0's and 1's (LEFT/RIGHT)
        prob_repeat: prob repeat previous trial being 0th NaN rather than 0.X
    """

    tm = np.array(tm)

    if randgen is None:
        randgen=np.random
    # since pybpod is just getting strs ok we'll just use str Nones
    if ss == 'None':
        ss = randgen.choice([0, 1])
    if sblock == 'None':  # this shouldnt happen inside the function because it's desirable to log first Block type
        sblock = randgen.choice(np.arange(tm.shape[0]))
    if block_seq == 'None':
        block_seq = np.tile(
            np.roll(np.arange(tm.shape[0]), -int(sblock)),
            int(bnum / tm.shape[0]) + tm.shape[0]
            )[:bnum]
    if block_lens == 'None':
        block_lens = np.repeat(blen, bnum)
    out = [ss]
    prob_repeat = []  # len = len(out)-1 !! # agree about how to stardartize this, leaving it as it was
    # first block having 1st arbitrary side
    for btype, curr_blen in zip(block_seq,
                                block_lens):  # ensure both zipped lists/arrays have the same length,
        # else it can lead to unexpected behavior will happen
        # print(f'iteration: {i}, block type: {btype}, blen: {curr_blen}')
        for x in range(curr_blen):
            blockProb = tm[btype]
            c_r_prob = blockProb[out[-1]]
            out += [randgen.choice([out[-1], (out[-1] - 1) ** 2], p=[c_r_prob, 1 - c_r_prob])]
            prob_repeat += [c_r_prob]

    return out, prob_repeat

# this could be replaced with newBlock([[.95,.95],[.05, .05]], ss=ss, **other_kwargs)
def block_easy(ss, repetitive=True, bsize=200, bnum=10, randgen=None):
    '''
    ss: int, starting side; 0=left; 1=right
    repetitive: bool, repetitve or alt block (prob 80/20 but swapped)
    bsize: blocksize (int)
    bnum: number of blocks to output (alternating from repetitive to alternating)
    returns 1-d array of 0 and 1 & prob repeat
    '''
    if randgen is None:
        randgen=np.random
    # some security checks
    if ss not in [0,1]:
        raise ValueError('ss: starting side, int, either 0 (left) or 1(right)')
    if isinstance(repetitive, bool) != True:
        raise ValueError('repetitive: bool, True->repetitive block; False-> alternating block')
    if isinstance(bsize, int)!=True:
        raise ValueError('bsize: int, blocksize')
    if not (isinstance(bnum, int) and bnum>=1 == True):
        raise ValueError('bnum: int <= 1')
    out = [ss]
    prob_repeat = []
    for _ in range(bnum):
        if repetitive == True:
            prob = [0.95, 0.05]
            prob_repeat = prob_repeat + [prob[0]]*bsize
        else:
            prob = [0.05, 0.95]
            prob_repeat = prob_repeat + [prob[0]]*bsize
        rep_alt_vec = randgen.choice([0,1], bsize, replace=True, p=prob) # being 0 repeat and 1 alt
        #generate L/R vec from starting point
        for i in range(len(rep_alt_vec)):
            out.append(abs(out[-1]-rep_alt_vec[i]))
        repetitive = not repetitive # once finished block, next one will be a different one
    return out, prob_repeat



def is_stim_fair(rewside, Lenv, Renv, freq=20 ,modwave=(1 * np.sin(2 * np.pi * (20) * np.arange(0, 1, step=1/48000) + np.pi)), stimdur=0.5):
    """
    # default modwave is wrong but whatever
    function to assess whether the rewarded side has higher chances to be the most intense
    rewside: side which is about to be rewarded (0: L, 1:R)
    Lenv = left envelope stairs [negative values]
    Renv = right envelope stairs
    modwave = actual envelope carrier wave
    stimdur = stim duration [total possible length according the soundvector generated]

    returns:
    av_fair= on average it's more likely to have a fair stim (without considering rats' RTs )
    resSoundmask = boolean array in ms scale where the cumulative stim is pointing to the rewarded side

    # adapt to make it more flexible if it's going to be used anyway
    """
    #envL = np.repeat(Lenv, int(stimdur*48000/20)) # 20 is freq
    #envR = np.repeat(Renv, int(stimdur*48000/20))
    #resSound = cumtrapz(modwave * np.repeat((Lenv+Renv), int(stimdur*48000/freq)), dx=(1/1000)*stimdur) # we sample at ms scale so we can index it later by int(ms)
    resSound = cumtrapz(modwave * np.repeat((Lenv+Renv), int(modwave.size/(freq*stimdur))), dx=(1/1000)*stimdur)

    if rewside==0:
        resSoundmask = (resSound<0)
    else:
        resSoundmask = (resSound>0) # generate boolean for the RTs where stim is fair

    #av_fair = (int(round((resSoundmask*1).mean())) == int(rewside)) # buggy, no need to compare to rewside because resSoundMask will be on average
    # true if its fair
    if resSoundmask.mean()>=0.5:
        return True, resSoundmask
    else:
        return False, resSoundmask


if 'SLACK_BOT_TOKEN' in os.environ:
    def slack_spam(msg='hey buddy', filepath=None, userid='U8J8YA66S'):
        """this sends msgs through the bot,
        avoid spamming too much else it will get banned/timed-out"""
        ids_dic = {
            'jordi': 'U8J8YA66S',
            'lejla': 'U7TTEEN4T',
            'dani': 'UCFMZDWE8',
            'yerko': 'UB3B8425D',
            'carles': 'UPZPM32UC'
        }

        if (userid[0]!='U') and (userid[0]!='#'): # asumes it is a first name
            try:
                userid = ids_dic[userid.lower()]
            except:
                raise ValueError('double-check slack channel id (receiver)')

        token = os.environ.get('SLACK_BOT_TOKEN')
        if token is None:
            print('no SLACK_BOT_TOKEN in environ')
            raise EnvironmentError('no SLACK_BOT_TOKEN in environ')
        else:
            try:
                client = slack.WebClient(token=token)
                if (os.path.exists(filepath)) and (filepath is not None):
                    response = client.files_upload(
                            channels=userid,
                            file=filepath,
                            initial_comment=msg)
                elif filepath is None:
                    response = client.chat_postMessage(
                        channel=userid,
                        text=msg)
            except Exception as e:
                print(e) # perhaps prints are caught by pybpod


def get_zt(
    hits, rewside=[], current_index=None, weights=None, gating_ae=0, expfunc=None, space='leftright',
    cumulative=True, N0=0.45, tau=1.55 #N0=0.76, tau=1.85
    ):
    '''
    hits: list/array of last hits(1) and misses(0), its length implicitly specifies window to calc Zt
    rewside: reward_side vector (0=left; 1=right) aka trial_list in our tasks jargon
    current_index: current trial(i), hence hits ~ complete_hithistory_up_to_trial_i[i-window.size : i]
    weights: if provided, used to weight each type of transition (T++, T+-, T-+, T--): shape(transition, window),
            else this will just take into account T++ transitions.
    gating: it multiplies current zt if after-error. ie to obtain a complete gating use 0, no gating 1
    expfunc: custom function to multiply (correct)transitions with | later == recent
    space: whether to return in 'leftright' or 'repalt' space

    TLDR: just fill hits, rewside, current index, and adapt gating_ae
    warning: by default these settings (just) consider T++ and T-+ transitions

    TODO: include option to add N0.size==4, one per transition; same with tau
    TODO: weights... (should be replaced by above todo)
    '''
    if isinstance(hits, list):
        hits = np.array(hits)

    rewside_w = rewside[current_index-hits.size:current_index]
    transitions = (np.diff(rewside_w)**2-1)**2 # size=hits-1 again # 1=rep, 0=alt
    transitions = transitions*2-1 # 1=rep; -1= alt

    if cumulative:
        if weights is None:
            zt = 0
            for i in range(1,hits.size):
                zt = hits[i-1]*hits[i]*transitions[i]*N0 + zt * np.exp(-1/tau)  # both trials (t and previous) must be correct
        else:
            raise NotImplementedError('weights not supported yet')

    elif weights is not None:
        if isinstance(weights, list):
            weights = np.array(weights)

        # TBD complete
        if weights.ndim == 1:
            pass
        elif weights.ndim == 2:
            pass
        else:
            raise ValueError('weight dimensions should be either 1 or 2')

        raise NotImplementedError('weights not supported yet')

    elif expfunc is None:
        # define here default exponential
        # fit real data to get parameters
        def exp_decay(t_, N0_=N0, tau_=tau):
            """other params calculated elsewhere
            t: transitions back"""
            return (N0_ * np.exp(-np.arange(t_) / tau_))[::-1] # returning flipped array

        zt = (hits[1:] * hits[:-1] * transitions * exp_decay(transitions.size)).sum() # multiply hits[:-1] * [1:] | to get correct transitions
    else:
        zt = (hits[1:] * hits[:-1] * transitions * expfunc(transitions.size)).sum() # same

    # get previous response
    prev_R = rewside[current_index-1] # prev trial rewarded side
    # apply after-error gating
    if not hits[-1]:
        zt *=  gating_ae
        prev_R = (prev_R-1)**2 # rat did an error chosing unrewarded side

    # transform to desired space
    if space=='repalt':
        pass
    elif space=='leftright':
        zt *= (prev_R*2-1)
    else:
        raise KeyError(f"posible space values are: 'leftright' and 'repalt'\nlooks like {space} is not among them")

    return zt

def get_zt_light(zt_1, hit, hit_1, rewside, rewside_1, N0=0.45, tau=1.55):
    """If you need to run it in each trial, former function becomes heavy and perhaps cumbersome
    this one is fast and gets stuff done when tracking ONLY T++
    zt_1: former zt estimate (in rep_alt space (-1~1))
    hit: current trial hit=1, else =0
    hit_1: idem but prev trial
    rewside: current resp (0=left; 1=Right)
    rewside_1: same but prev trial

    returns: tuple: zt (rep_alt) ,zt (Left_right)
    """

    resp, resp_1 = rewside*2-1, rewside_1*2-1 # -1 ~ 1 space
    if not hit:
        resp *= -1 # rat response opposite to rewarded side
    if not hit_1:
        resp_1 *= -1 # same but 1 trial back

    zt = zt_1 * np.exp(-1/tau)

    if hit and hit_1: # T++ transition
        trans = resp* resp_1 # 1: rep; -1: alt
        zt += trans*N0

    return zt, zt*resp
