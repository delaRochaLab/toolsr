from toolsR.utils.pulse_pal_object import PulsePalObject

class PulsePal:
    def __init__(self, address, channel=1):
        self.com = PulsePalObject()
        self.com.connect(address)
        self.channel = channel
        self.com.programTriggerChannelParam('triggerMode', channel, 0)
        self.com.isBiphasic[1:5] = [0] * 4
        self.com.phase1Voltage[1:5] = [10] * 4
        self.com.restingVoltage[1:5] = [0] * 4
        self.com.syncAllParams()
        self.pulse_width = [0, 0, 0]
        self.c1, self.c2, self.c3, self.c4 = 0, 0, 0, 0
        if self.channel == 1:
            self.c1 = 1
        elif self.channel == 2:
            self.c2 = 2
        elif self.channel == 3:
            self.c3 = 1
        else:
            self.c4 = 1

    def disconnect(self):
        self.com.disconnect()

    # assigning the pulses takes aprox 50 ms for each second of duration of the pulse
    # so you need to assign the pulses at the beggining of the task
    # there can be 2 pulses stored at the same time: pulse_number=1 and pulse_number=2
    def assign_pulse(self, pulse, pulse_number):
        if pulse_number not in [1, 2]:
            print('you can only use pulse_number 1 or 2')
            raise
        pulse_width = pulse[0]
        voltages = pulse[1]
        self.com.sendCustomWaveform(pulse_number, pulse_width, voltages)
        self.pulse_width[pulse_number] = pulse_width

    # this is super fast, less than 1 ms
    def trigger_pulse(self, pulse_number):
        self.com.programOutputChannelParam('customTrainID', self.channel, pulse_number)
        self.com.programOutputChannelParam('phase1Duration', self.channel, self.pulse_width[pulse_number])
        self.com.abortPulseTrains()
        self.com.triggerOutputChannels(self.c1, self.c2, self.c3, self.c4)

    def stop_pulse(self):
        self.com.abortPulseTrains()


    # methods to create different types of pulses
    # need to return (pulse_width=float, voltages=[floats])
    @staticmethod
    def create_square_pulse(duration, duration_ramp_in, duration_ramp_off, voltage, samples_per_second= 1000):
        duration_samples = int(duration * samples_per_second)
        duration_ramp_in_samples = duration_ramp_in * samples_per_second
        duration_ramp_off_samples = duration_ramp_off * samples_per_second
        start_ramp_off_samples = duration_samples - duration_ramp_off_samples
        voltages = list(range(0, duration_samples))
        for i in voltages:
            if i < duration_ramp_in_samples:
                voltages[i] = i * voltage / duration_ramp_in_samples
            elif i > start_ramp_off_samples:
                voltages[i] = voltage - (i - start_ramp_off_samples) * voltage / duration_ramp_off_samples
            else:
                voltages[i] = voltage
        pulse_width = 1 / samples_per_second
        pulse = (pulse_width, voltages)
        return pulse


    @staticmethod
    def create_square_pulsetrain(duration, time_on, time_off, voltage, samples_per_second= 1000):
        duration_samples = int(duration * samples_per_second)
        voltages = list(range(0, duration_samples))
        samples_on = time_on * samples_per_second
        samples_off = time_off * samples_per_second
        samples_pulse = samples_on + samples_off
        for i in voltages:
            j = i % samples_pulse
            if j < samples_on:
                voltages[i] = voltage
            else:
                voltages[i] = 0
        pulse_width = 1 / samples_per_second
        pulse = (pulse_width, voltages)
        return pulse
