from time import gmtime, strftime
import cv2
from multiprocessing import Process, JoinableQueue
import numpy as np
from datetime import datetime
from toolsR.utils import TimerR
from toolsR.VideoTracking.algorithm import Algorithm
import os

try:  # not all setups have PIL
    from PIL import Image
    from PIL import ImageDraw
except:
    pass


killing = False


class VideoR(Process):

    def __init__(self, indx_or_path='0', name_video=None, path=None, width=None, height=None, fps=None,
                 codec_cam='X264', codec_video='X264', showWindows=True, title='def title', tracking=False, nAreas=3,
                 bpod_address=None, cam_sync=None, states_on_sync=False, mask=False):

        Process.__init__(self)
        self.idx = indx_or_path
        self.name_video = name_video
        self.path = path
        self.width = width
        self.height = height
        self.fps = fps
        self.showWindows = showWindows
        self.title = title
        self.fourcc_cam = cv2.VideoWriter_fourcc(*codec_cam)
        self.fourcc_out = cv2.VideoWriter_fourcc(*codec_video)

        self.isSaving = False  # is True only when start to record
        self.recordV = ''
        self.frame = []
        self.frame_counter = 0
        self.timestamps = []
        self.frames = []
        self.states = []

        self.cam_sync = cam_sync if cam_sync is not None else {}
        self.mask = mask

        cam_values = list(self.cam_sync.values())
        try:
            if isinstance(cam_values[0], list):
                keys = [item[0] for item in cam_values]
                values = [item[2] for item in cam_values]
                self.cam_sync = dict(zip(keys, values))
        except IndexError:
            pass

        self.states_on_sync = states_on_sync
        self.sync = False if cam_sync is None else True
        self.cam_sync2 = {}
        self.list_states = []
        self.list_state_circle0 = []
        self.list_state_circle1 = []

        i = 0
        keys = sorted(self.cam_sync)
        for key in keys:
            self.list_states.append(key)
            value = self.cam_sync[key]
            self.list_state_circle0.append(value[0])
            self.list_state_circle1.append(value[1])

            self.cam_sync2[key] = i
            i += 1

        self.command_queue = JoinableQueue()

        self.video = None
        self.out_video = None  # It opens a channel to save the video only when the variable isSaving=true

        self.tracking = tracking
        self.nAreas = nAreas
        self.address = bpod_address
        self.alg = Algorithm(self.nAreas, self.address)

    def play(self):
        self.start()

    def record(self):
        self.command_queue.put('record')

    def pause(self):
        self.command_queue.put('pause')

    def stop(self):
        self.command_queue.put('stop')
        self.join()
        self.video = None

    def put_state(self, state: str):
        self.command_queue.put(state)

    def run(self):
        global killing

        path_video = '/dev/video-' + str(self.idx)

        if os.path.exists(str(self.idx)):
            self.video = cv2.VideoCapture(str(self.idx))
        elif os.path.exists(path_video):
            self.video = cv2.VideoCapture(path_video)
        else:
            self.video = cv2.VideoCapture(int(self.idx))

        self.video.set(cv2.CAP_PROP_FOURCC, self.fourcc_cam)

        if self.width is not None:
            self.video.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
        if self.height is not None:
            self.video.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height)
        if self.fps is not None:
            self.video.set(cv2.CAP_PROP_FPS, self.fps)

        self.fps = int(self.video.get(cv2.CAP_PROP_FPS))
        self.width = int(self.video.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(self.video.get(cv2.CAP_PROP_FRAME_HEIGHT))

        if self.name_video is None:
            self.name_video = strftime("%d%b%Y_%H%M%S.mkv", gmtime())
        if self.path is None:
            self.path = ''

        self.frame = np.zeros([self.height, self.width])

        chrono = TimerR()

        # fps counter
        fps_now = 0
        counter_fps = 0
        last_sec = 0

        # Start
        next_command = 'play'
        state = ''
        previous_state = ''

        while True:
            # Get the command
            if not self.command_queue.empty():
                next_command = self.command_queue.get()

            # Check the command and change the variables
            if next_command == 'record':
                self.recordV = 'record'
                if not self.isSaving:
                    self.out_video = cv2.VideoWriter(self.path + self.name_video, self.fourcc_out, self.fps,
                                                     (self.width, self.height))
                    self.isSaving = True
            elif next_command == 'pause':
                chrono.pause()
                self.recordV = 'pause'
            elif next_command == 'stop':
                if chrono.isRunning():  # close the timer
                    chrono.stop()
                    chrono.reset()
                self.recordV = 'stop'
            elif next_command != 'play':
                state = next_command

            if self.recordV == 'stop':
                break
            elif killing:
                self.close()
            else:
                try:
                    (a, frame), ctime = self.video.read(), datetime.now()
                    if a:
                        if self.recordV == 'record':
                            self.out_video.write(frame)  # self.outVideo.write(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY))
                            self.timestamps += [ctime]
                            self.frame_counter += 1
                            counter_fps += 1

                            if not chrono.isRunning() or chrono.isPause():  # Start the chrono
                                chrono.start()

                            now_sec = int(chrono.getTimeSeconds())
                            if now_sec % 5 == 0 and now_sec != last_sec:  # reset the fps counter each 5 sec
                                last_sec = int(now_sec)
                                fps_now = int(counter_fps / 5)
                                counter_fps = 0

                            cv2.putText(frame, 'fps: ' + str(fps_now) + ' time: ' + chrono.getString() + ' Rec',
                                        (10, 30),
                                        cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
                            if self.tracking:
                                mask, frame = self.alg.apply(frame)

                            if self.sync:
                                try:
                                    circle = self.cam_sync[state]
                                    cv2.circle(frame, circle, 12, (255, 255, 255), -1)
                                    if self.states_on_sync:
                                        cv2.putText(frame, f'{state}', (450, 35),
                                                    cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
                                except:
                                    pass

                                if state != previous_state:
                                    self.frames += [self.frame_counter]
                                    self.states += [self.cam_sync2[state]]

                        elif self.recordV == 'pause':
                            if chrono.isRunning():
                                cv2.putText(frame, 'fps: ' + str(fps_now) + ' time: ' + chrono.getString() + ' Pause',
                                            (10, 30), cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
                        if self.showWindows:
                            if self.mask:
                                img = Image.fromarray(frame)
                                draw = ImageDraw.Draw(img)
                                draw.line([(319, 0), (319, 480)], fill=(0,0,0), width=3)
                                draw.arc((290,180,350,240), 180, 0,'black', width=2)
                                draw.ellipse((245,135,280,170), outline='black', width=2)
                                draw.ellipse((355,135,390,170), outline='black', width=2)
                                cv2.imshow("Streaming Camera + Mask", np.array(img))
                            else:
                                cv2.imshow(self.title, frame)
                except:
                     pass


            # Exit if ESC pressed
            if self.showWindows:
                k = cv2.waitKey(1) & 0xff
                if k == 27:
                    break

        self.close()
        return

    def close(self):  # release the camera and close the windows, then save the file.
        global killing
        self.video.release()
        cv2.destroyAllWindows()
        if self.out_video is not None:
            self.out_video.release()
            timestamps = np.array(self.timestamps, dtype='datetime64[us]')
            if self.sync:
                frames = np.array(self.frames)
                states = np.array(self.states)
                list_states = np.array(self.list_states)
                list_circles0 = np.array(self.list_state_circle0)
                list_circles1 = np.array(self.list_state_circle1)
                np.savez(f'{self.path}{self.name_video[:-4]}.npz',
                         timestamps, frames, states, list_states, list_circles0, list_circles1)
            else:
                np.save(f'{self.path}{self.name_video[:-4]}.npy', timestamps)
            print('VideoR: Video saved.')
            print(f'Total Frames = {self.frame_counter}, total timestamps={len(self.timestamps)}')
            if killing:
                raise()

    @staticmethod
    def _decode_fourcc(v):
        v = int(v)
        return "".join([chr((v >> 8 * i) & 0xFF) for i in range(4)])
