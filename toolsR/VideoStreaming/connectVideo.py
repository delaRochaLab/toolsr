# !/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import sys

from toolsR.VideoStreaming.videoRecord import VideoR


def _start(arg):

    path = arg.p

    # Control
    if path == 'None':
        path = ''

    nv = arg.n
    if nv == 'None':
        nv = None

    w = arg.w
    if w == 'None':
        w = None
    else:
        w = int(w)

    ht = arg.g
    if ht == 'None':
        ht = None
    else:
        ht = int(ht)

    fps = arg.f
    if fps == 'None':
        fps = None
    else:
        fps = int(fps)

    # Run
    video = VideoR(arg.i, nv, path, w, ht, fps, arg.c, arg.d, arg.s)

    while True:
        data = sys.stdin.readline()
        if data == "P\n":
            video.play()
        elif data == "R\n":
            video.record()
        elif data == "P\n":
            video.pause()
        elif data == "S\n":
            video.stop()
            break

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", type=int,
                        help="Index camera")
    parser.add_argument("-n", type=str,
                        help="Name of the video")
    parser.add_argument("-p", type=str,
                        help="Path where to save the video")
    parser.add_argument("-w", type=str,
                        help="Width camera")
    parser.add_argument("-g", type=str,
                        help="Height camera")
    parser.add_argument("-f", type=str,
                        help="Frame per seconds")
    parser.add_argument("-c", type=str,
                        help="Codec camera")
    parser.add_argument("-d", type=str,
                        help="Codec video")
    parser.add_argument("-s", type=bool,
                        help="Show display")
    args = parser.parse_args()

    _start(args)