# !/usr/bin/python3
# -*- coding: utf-8 -*-

__version__ = "3.0"
__author__ = "Rachid Azizi"
__credits__ = ["Rachid Aizi"]
__license__ = "Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>"
__maintainer__ = ["Rachid Azizi"]
__email__ = ["azi92rach@gmail.com"]
__status__ = "Development"

try:
    from toolsR.Sound.soundStream import SoundR
except ImportError:
    pass

try:
    from toolsR.Sound.soundStream import SoundR2
except ImportError:
    pass

try:
    from toolsR.Sound.soundStream import SoundR3
except ImportError:
    pass

try:
    from toolsR.Sound.soundStream import SoundROld
except ImportError:
    pass

try:
    from toolsR.VideoStreaming.videoRecord import VideoR
except ImportError:
    pass

try:
    from toolsR.VideoStreaming.videoRecordpopen import VideoRP
except ImportError:
    pass

try:
    from toolsR.VideoTracking.Tracking import TrackingR
except ImportError:
    pass

try:
    from toolsR.utils.SoftcodeConnection import SoftcodeConnection
except ImportError:
    pass

try:
    from toolsR.utils.organizeData import DataR
except ImportError:
    pass

try:
    from toolsR.utils.timer import TimerR
except ImportError:
    pass

try:
    from toolsR.utils import UtilsR
except ImportError:
    pass

try:
    from toolsR.Touchscreen.touchscreen import TouchscreenR
except ImportError:
    pass

try:
    from toolsR.Touchscreen.touch import touchR
except ImportError:
    pass

try:
    from toolsR.utils import pulse_pal
except ImportError:
    pass
