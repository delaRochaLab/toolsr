# IMPORTS
import evdev
import numpy as np
from select import select
from datetime import datetime
from threading import Thread, Event
import time

# TIMER
class Timer:

    def __init__(self, timeup):
        self.timeup = timeup  # in seconds
        self.initial_time = datetime.now()

    def reset(self):
        self.initial_time = datetime.now()

    @property
    def remaining_time(self):
        elapsed_time = datetime.now() - self.initial_time
        seconds = self.timeup - elapsed_time.total_seconds()
        if seconds < 0:
            return 0
        else:
            return seconds


class Recorder(Thread):

    def __init__(self, touch_device, timeup, only_x, first_touch):
        Thread.__init__(self)

        self.answer_sent = Event()
        self.get_answer = Event()

        self.only_x = only_x
        self.first_touch = first_touch

        self.trial_answer = None
        self.daemon = True

        self.timeup = timeup

        self.device = evdev.InputDevice(touch_device)
        self.device.grab()
        
        self.timer = None


    def run(self):
        while True:
            self.get_answer.wait()
            if self.get_answer.is_set():
                self.get_coords()

    def close(self):
        self.device.ungrab()
        pass

    def start_timer(self):
        self.timer = Timer(self.timeup)

    def get_coords(self):

        x_coord = None
        y_coord = None
        answer = None

        try:
            while self.device.read_one() is not None:  # clearing buffer of events
                pass
        except:
            print('lectureError in clearing buffer')
            pass

        reading_coords = True

        while reading_coords:
            ready, _, _ = select([self.device], [], [], self.timer.remaining_time)

            if ready:
                try:
                    for event in self.device.read():
                        if event.type == evdev.ecodes.EV_ABS:  # if event is a coordinate
                            if event.code == 0 or event.code == 53:  # x coord
                                x_coord = event.value
                            if event.code == 1 or event.code == 54:  # y coord
                                y_coord = event.value
                            if self.first_touch and x_coord is not None and (y_coord is not None or self.only_x):
                                answer = [x_coord, y_coord]
                                reading_coords = False
                                break
                        elif event.type == evdev.ecodes.EV_KEY and event.value != 1:  # BTN_TOUCH up
                            if not self.first_touch and x_coord is not None and (y_coord is not None or self.only_x):
                                answer = [x_coord, y_coord]
                                reading_coords = False
                                break
                except:
                    print('lectureError')
                    break
            else:
                reading_coords = False
                break


        if answer is None:
            self.trial_answer = 'miss'
        else:
            self.trial_answer = answer
        self.answer_sent.set()
        self.get_answer.clear()
