# IMPORTS
import threading
import numpy as np
from numpy import linalg as ln
# from psychopy import visual
from toolsR.Touchscreen.recorder import Recorder
from toolsR.utils.SoftcodeConnection import SoftcodeConnection
import time


class TouchscreenR:
    """
    Docs.
    """

    def __init__(self, screen, color, win_resolution, touch_resolution, touch_device,
                 x_position, y_position, threshold, width, timeup, only_x, first_touch):

        self.softcode = SoftcodeConnection()

        view_pos = [-int(win_resolution[0] / 2), int(win_resolution[1] / 2)]

        self.win = visual.Window(win_resolution, screen=screen,  color=color, units='pix',
                                 fullscr=True, viewPos=view_pos)
        self.win_resolution = win_resolution
        self.touch_resolution = touch_resolution
        self.threshold = threshold
        self.width = width
        self.x_position = x_position
        self.y_position = y_position
        self.last_answer = None

        self.only_x = only_x
        self.first_touch = first_touch

        self.recorder = Recorder(touch_device, timeup, only_x, first_touch)

        self.stim_time = None
        self.stim_timer = None

        self.changes_allowed = True

        self.recorder.start()

    # FUNCTIONS
    def show_stim(self):
        """
        Tell psychopy to show the stimulus for a given duration.
        """
        while True:
            if self.changes_allowed:
                self.changes_allowed = False
                self.recorder.timer = None
                try:
                    self.stim_timer.cancel()
                except:
                    pass

                if self.only_x:
                    stim = visual.Rect(win=self.win, width=self.width, height=self.win_resolution[1], lineColor='black',
                                       fillColor='white', units='pix', pos=[self.x_position, self.y_position])
                else:
                    stim = visual.Circle(win=self.win, radius=self.width / 2, edges=32, lineColor='black',
                                         fillColor='white', units='pix', pos=[self.x_position, self.y_position])

                stim.draw()
                self.win.flip()
                self.stim_timer = threading.Timer(self.stim_time, self.black_screen)
                self.stim_timer.daemon = True
                self.stim_timer.start()
                self.changes_allowed = True
                break

    def black_screen(self):
        """
        Tell psychopy to turn black the screen hiding all the
        stimulus or windows previously created.
        """
        print('BLACK')
        while True:
            if self.changes_allowed:
                self.changes_allowed = False
                try:
                    self.stim_timer.cancel()
                except:
                    pass
                self.win.flip()
                self.changes_allowed = True
                break

    def gray_screen(self):
        """
        Tell psychopy to turn gray the whole screen.
        """
        time.sleep(0.05)
        while True:
            if self.changes_allowed:
                self.changes_allowed = False
                try:
                    self.stim_timer.cancel()
                except:
                    pass
                pos = [int(self.win_resolution[0] / 2), -int(self.win_resolution[1] / 2)]

                stim = visual.Rect(win=self.win, width=self.win_resolution[0], height=self.win_resolution[1],
                                   fillColor=[0, 0, 0], units='pix', pos=pos)
                stim.draw()
                self.win.flip()
                self.changes_allowed = True
                break

    def finish_recording(self):
        self.recorder.get_answer.clear()

    def stop(self):
        """
        Stop the touchscreen.
        """
        self.win.close()
        self.recorder.close()
        self.recorder = None

    def wait_response(self):
        """
        Set thread event to track response.
        """
        if self.recorder.timer is None:
            self.recorder.start_timer()
        self.recorder.get_answer.set()
        self.recorder.answer_sent.wait()
        self.recorder.answer_sent.clear()
        self.last_answer = self.recorder.trial_answer

        xpsy = abs(self.x_position)
        ypsy = abs(self.y_position)

        if self.last_answer == 'miss':
            self.softcode.send(3)
            self.softcode.send(3)
        elif self.last_answer is not None:
            xtouch = abs(self.last_answer[0] * (self.win_resolution[0] / self.touch_resolution[0]))
            try:
                ytouch = abs(self.last_answer[1] * (self.win_resolution[1] / self.touch_resolution[1]))
            except:
                ytouch = None

            if self.only_x:
                if abs(xtouch - xpsy) < self.threshold / 2:
                    self.softcode.send(1)
                    self.stim_timer.cancel()
                else:
                    self.softcode.send(2)
            else:
                if ln.norm(np.array((xtouch, ytouch)) - np.array((xpsy, ypsy))) < self.threshold / 2:
                    self.softcode.send(1)
                    self.stim_timer.cancel()
                else:
                    self.softcode.send(2)

            if ytouch is None:
                ytouch = 0

            return [xtouch, ytouch]
