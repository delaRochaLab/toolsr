from multiprocessing import Process, Value
import sounddevice as sd
import numpy as np
import time
from threading import Thread
from timeit import default_timer as timer


class SoundR:
    def __init__(self, sampleRate=44100, deviceOut='default', channelsOut=2, latency='low'):

        try:
            devices = self.getDevicesNamesAndNumbers()
            device = devices[deviceOut]
        except:
            device = deviceOut

        sd.default.device = device
        sd.default.samplerate = sampleRate
        sd.default.latency = latency
        sd.default.channels = channelsOut

        self._Stream = sd.OutputStream(dtype='float32')
        self._Stream.close()
        self._sound = []
        self._playing = Value('i', 0)
        self._p = Process(target=self._play_sound_background)
        self._p.daemon = True

    @staticmethod
    def getDevices():
        return sd.query_devices()

    @staticmethod
    def getDevicesNamesAndNumbers():
        devi = sd.query_devices()

        idx = 0
        idxs = []

        for dev in devi:
            if dev['name'].startswith('Xonar DX: Multichannel'):
                idxs.append(idx)
            idx += 1

        idx = 0
        names = []

        with open('/proc/asound/cards') as file:
            line = file.readline()
            while line:
                if '[' in line and ']' in line:
                    try:
                        a = line.index('BPOD')
                        names.append(line[a:a+6])
                    except:
                        try:
                            a = line.index('bpod')
                            names.append(line[a:a + 6])
                        except:
                            pass
                    idx += 1
                line = file.readline()

        return dict(zip(names, idxs))

    def load(self, v1, v2=None):
        """ Load audio """
        if v2 is None:
            v2 = v1
        if len(v1) != len(v2):
            raise ValueError('SoundR: The length of the vectors v1 and v2 has be the same.')
        try:
            self.stopSound()
        except AttributeError:
            pass
        sound = self._create_sound_vec(v1, v2)
        self._Stream.close()
        self._Stream = sd.OutputStream(dtype='float32')
        self._Stream.start()
        self._sound = sound
        self._playing.value = 0
        self._p = Process(target=self._play_sound_background)
        self._p.daemon = True
        self._p.start()
        print('SoundR: Loaded.')

    def playSound(self):
        if self._sound == []:
            raise ValueError('SoundR: No sound loaded. Please, use the method load().')
        self._playing.value = 1

    def stopSound(self):
        try:
            if self._playing.value == 1:
                self._playing.value = 2
                self._Stream.close()
                self._p.terminate()
                print('SoundR: Stop.')
            elif self._playing.value == 0:
                self._playing.value = 2
        except AttributeError:
            print('SoundR: it is not possible to stop. No process is running.')

    def _play_sound_background(self):
        while True:
            if self._playing.value == 1:
                print('SoundR: Play.')
                if self._sound == []:
                    print('Error: no sound is loaded.')
                    self._playing.value = 2
                    break
                else:
                    self._Stream.write(self._sound)
                    self._playing.value = 2
                    break
            elif self._playing.value == 2:
                break

    @staticmethod
    def _create_sound_vec(v1, v2):
        sound = np.array([v1, v2])  # left and right channel
        return np.ascontiguousarray(sound.T, dtype=np.float32)


class SoundR2:
    def __init__(self, sampleRate=44100, deviceOut='default', channelsOut=2, latency='low'):

        try:
            devices = self.getDevicesNamesAndNumbers()
            device = devices[deviceOut]
        except:
            device = deviceOut

        sd.default.device = device
        sd.default.samplerate = sampleRate
        sd.default.latency = latency
        sd.default.channels = channelsOut

        self._Stream = sd.OutputStream(dtype='float32')
        self._Stream.close()
        self._sound = []
        self._playing = Value('i', 0)
        self._p = Process(target=self._play_sound_background)
        self._p.daemon = True

    @staticmethod
    def getDevices():
        return sd.query_devices()

    @staticmethod
    def getDevicesNamesAndNumbers():
        devi = sd.query_devices()

        idx = 0
        idxs = []

        for dev in devi:
            if dev['name'].startswith('Xonar DX: Multichannel'):
                idxs.append(idx)
            idx += 1

        idx = 0
        names = []

        with open('/proc/asound/cards') as file:
            line = file.readline()
            while line:
                if '[' in line and ']' in line:
                    try:
                        a = line.index('BPOD')
                        names.append(line[a:a+6])
                    except:
                        try:
                            a = line.index('bpod')
                            names.append(line[a:a + 6])
                        except:
                            pass
                    idx += 1
                line = file.readline()

        return dict(zip(names, idxs))

    def load(self, v1, v2=None):
        """ Load audio """
        if v2 is None:
            v2 = v1
        if len(v1) != len(v2):
            raise ValueError('SoundR: The length of the vectors v1 and v2 has be the same.')
        try:
            self.stopSound()
        except AttributeError:
            pass
        sound = self._create_sound_vec(v1, v2)
        self._Stream.close()
        self._Stream = sd.OutputStream(dtype='float32')
        self._Stream.start()
        self._sound = sound
        self._playing.value = 0
        self._p = Process(target=self._play_sound_background)
        self._p.daemon = True
        self._p.start()
        print('SoundR: Loaded.')

    def playSound(self):
        if self._sound == []:
            raise ValueError('SoundR: No sound loaded. Please, use the method load().')
        self._playing.value = 1

    def stopSound(self):
        try:
            if self._playing.value == 1:
                self._playing.value = 2
                self._Stream.close()
                self._p.terminate()
                print('SoundR: Stop.')
            elif self._playing.value == 0:
                self._playing.value = 2
        except AttributeError:
            print('SoundR: it is not possible to stop. No process is running.')

    def _play_sound_background(self):
        while True:
            if self._playing.value == 1:
                print('SoundR: Play.')
                if self._sound == []:
                    print('Error: no sound is loaded.')
                    self._playing.value = 2
                    break
                else:
                    self._Stream.write(self._sound)
                    self._playing.value = 2
                    break
            elif self._playing.value == 2:
                break

    @staticmethod
    def _create_sound_vec(v1, v2):
        sound = np.array([v1, v2])  # left and right channel
        return np.ascontiguousarray(sound.T, dtype=np.float32)


class SoundR3:
    def __init__(self, sampleRate=44100, deviceOut='default', channelsOut=2, latency='low',
                 blocksize_ms=0.5, ramp_ms=0, delay_one_block=False):

        try:
            devices = self.getDevicesNamesAndNumbers()
            self.device = devices[deviceOut]
        except:
            self.device = deviceOut

        self.samplerate = sampleRate
        self.channels = channelsOut
        self.latency = latency
        self.blocksize = int(sampleRate * blocksize_ms / 1000)
        self.ramp = max(0, int(sampleRate * ramp_ms / 1000))
        if delay_one_block:
            self.delay = self.blocksize
        else:
            self.delay = 0
        self.duration = 0
        self.idx0 = 0
        self.idx = 0
        self.idx_ramp = 0
        self.status = 0  # 0 not loaded, 1 loaded, 2 playing
        self.ramp_vector = []
        self.sound = []

        self.time0 = 0
        self.start_point = 0
        self.stop_point = 0

        self.play_message = False
        self.stop_message = False

        self.stream = None

        sd.default.device = self.device
        sd.default.samplerate = self.samplerate
        sd.default.channels = self.channels
        sd.default.latency = self.latency
        sd.default.blocksize = self.blocksize

    def load(self, v1, v2=None):
        """ Load audio """
        if v2 is None:
            v2 = v1
        if len(v1) != len(v2):
            raise ValueError('SoundR: The length of the vectors v1 and v2 has be the same.')

        self._create_sound_vec(v1, v2)

        self.idx = 0
        self.idx0 = 0
        self.idx_ramp = 0
        self.start_point = 0
        self.stop_point = 0
        self.time0 = 0
        self.stop_message = False
        self.play_message = False
        self.status = 1
        self.stream = sd.OutputStream(device=self.device,
                                      channels=self.channels,
                                      callback=self.callback,
                                      samplerate=self.samplerate)
        self.stream.start()
        print('SoundR: Loaded.')

    def callback(self, outdata, frame_count, time_info, status):

        if status:
            print(status)

        if self.play_message:
            self.play_message = False
            self.start_point = min(int((timer() - self.time0) * self.samplerate), self.delay, frame_count)
            # print('start point', self.start_point)

        if self.stop_message:
            self.stop_message = False
            self.stop_point = min(int((timer() - self.time0) * self.samplerate), self.delay, frame_count)
            # print('stop point', self.stop_point)

            if self.idx < self.duration - self.ramp - self.stop_point:
                self.idx_ramp = self.duration - self.ramp - self.stop_point
                self.duration = int(self.idx + self.ramp + self.stop_point)

        remainder = self.duration - self.idx

        if self.status < 2:
            outdata[:] = 0
            self.idx0 += frame_count

        elif remainder == 0:
            self.status = 0
            self.idx = 0
            self.idx_ramp = 0
            raise sd.CallbackStop

        elif self.idx == 0:

            self.idx = self.start_point

            valid_frames = min(frame_count, remainder)

            sound = self.sound[self.idx:valid_frames]
            ramp = self.ramp_vector[self.idx:valid_frames]

            outdata[:self.idx] = 0
            outdata[self.idx:valid_frames] = sound * ramp
            outdata[valid_frames:] = 0
            self.idx += valid_frames
            self.idx_ramp = self.idx

        else:

            valid_frames = min(frame_count, remainder)

            sound = self.sound[self.idx:self.idx + valid_frames]
            ramp = self.ramp_vector[self.idx_ramp:self.idx_ramp + valid_frames]

            # print(self.idx, len(self.sound), duration, len(sound), len(ramp), valid_frames)

            outdata[:valid_frames] = sound * ramp
            outdata[valid_frames:] = 0
            self.idx += valid_frames
            self.idx_ramp += valid_frames

    def playSound(self):
        if self.status == 0:
            print('SoundR: cannot play, no sound is loaded')
        elif self.status == 2:
            print('SoundR: cannot play, sound is being played already')
        if self.status == 1:
            print('SoundR: Play')
            self.time0 = timer()
            self.play_message = True
            self.status = 2

    def stopSound(self):
        if self.status == 0:
            print('SoundR: cannot stop, no sound being played')
        elif self.status == 1:
            print('SoundR: cannot stop, no sound being played')
        elif self.status == 2:
            print('SoundR: Stop')
            self.time0 = timer()
            self.stop_message = True

    def _create_sound_vec(self, v1, v2):
        v0 = np.repeat(0, self.delay)
        v1 = np.concatenate([v0, v1])
        v2 = np.concatenate([v0, v2])
        sound = np.array([v1, v2])  # left and right channel
        self.sound = np.ascontiguousarray(sound.T, dtype=np.float32)
        self.duration = len(self.sound)
        if self.ramp > 0:
            ramp_on = np.arange(0, 1, 1 / self.ramp)
            ramp_no = np.repeat(1, self.duration - 2 * self.ramp - self.delay)
            ramp_off = np.flipud(ramp_on)
            ramp_vector = np.concatenate([v0, ramp_on, ramp_no, ramp_off])
        else:
            ramp_vector = np.repeat(1, self.duration)
        ramp_vector = np.array([ramp_vector, ramp_vector])
        self.ramp_vector = np.ascontiguousarray(ramp_vector.T, dtype=np.float32)

    @staticmethod
    def getDevices():
        return sd.query_devices()

    @staticmethod
    def getDevicesNamesAndNumbers():
        devi = sd.query_devices()

        idx = 0
        idxs = []

        for dev in devi:
            if dev['name'].startswith('Xonar DX: Multichannel'):
                idxs.append(idx)
            idx += 1

        idx = 0
        names = []

        with open('/proc/asound/cards') as file:
            line = file.readline()
            while line:
                if '[' in line and ']' in line:
                    try:
                        a = line.index('BPOD')
                        names.append(line[a:a + 6])
                    except:
                        try:
                            a = line.index('bpod')
                            names.append(line[a:a + 6])
                        except:
                            pass
                    idx += 1
                line = file.readline()

        return dict(zip(names, idxs))


class SoundROld:

    def __init__(self, sampleRate=44100, deviceOut='default', channelsOut=2, latency='low'):

        try:
            devices = self.getDevicesNamesAndNumbers()
            device = devices[deviceOut]
        except:
            device = deviceOut

        sd.default.device = device
        sd.default.samplerate = sampleRate
        # sd.default.blocksize = 384
        sd.default.latency = latency
        sd.default.channels = channelsOut

        self._Stream = sd.OutputStream(dtype='float32')
        self._Stream.close()
        self._sound = []

    @staticmethod
    def getDevices():
        return sd.query_devices()

    @staticmethod
    def getDevicesNamesAndNumbers():
        devi = sd.query_devices()

        idx = 0
        idxs = []

        for dev in devi:
            if dev['name'].startswith('Xonar DX: Multichannel'):
                idxs.append(idx)
            idx += 1

        idx = 0
        names = []

        with open('/proc/asound/cards') as file:
            line = file.readline()
            while line:
                if '[' in line and ']' in line:
                    try:
                        a = line.index('BPOD')
                        names.append(line[a:a+6])
                    except:
                        try:
                            a = line.index('bpod')
                            names.append(line[a:a + 6])
                        except:
                            pass
                    idx += 1
                line = file.readline()

        return dict(zip(names, idxs))

    def load(self, v1, v2=None):
        """ Load audio """
        if v2 is None:
            v2 = v1
        if len(v1) != len(v2):
            raise ValueError('SoundR: The length of the vectors v1 and v2 has be the same.')
        try:
            if self._p.is_alive():
                print('SoundR: It is not possible to load. The process is running. Wait! ')
                return
        except AttributeError:
            pass
        sound = self._createSoundVec(v1,v2)
        self._Stream.close()
        self._Stream = sd.OutputStream(dtype='float32')
        self._Stream.start()
        self._sound = sound
        self._p = Process(target=self._playSoundBackground)

    def playSound(self):
        if self._sound == []:
            raise ValueError('SoundR: No sound loaded. Please, use the method load().')
        if self._p.is_alive():
            print('SoundR: It is not possible to play. The process is running. Wait!')
            return
        try:
            self._p.start()
        except AttributeError:
            print('SoundR: The process is not present. Recall the function to fix the problem.')
        except AssertionError:
            print('SoundR: Load the sound again before to use playSound().')


    def stopSound(self):
        try:
            if self._p.is_alive():
                #self.Stream.abort()
                self._Stream.close()
                self._p.terminate()
                print('SoundR: Stop.')
            else:
                print('SoundR: it is not possible to stop. No process is running.')
        except AttributeError:
            print('SoundR: it is not possible to stop. No process is running.')

    def _playSoundBackground(self):
        print('SoundR: Play.')
        if self._sound == []:
            print('Error: no sound is loaded.')
        else:
            self._Stream.write(self._sound)

    def _createSoundVec(self, v1, v2):
        sound = np.array([v1, v2])  # left and right channel
        return np.ascontiguousarray(sound.T, dtype=np.float32)
