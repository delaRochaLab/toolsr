# !/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from toolsR import SoundR
import time

# TEST SOUND
def start():
    # Generate your sound arrays
    toneDuration = 1  # sec
    amplitude = 1.
    FsOut = 44100  # sample rate, depend on the sound card
    tvec = np.linspace(0, toneDuration, toneDuration * FsOut)
    fs = 1000
    s1 = amplitude * np.sin(2 * np.pi * fs * tvec)  # sound vector
    s2 = np.zeros(s1.size)  # empty sound

    # Create the sound server
    # ----> Print the list of your devices
    print(SoundR.getDevices())
    # Choose from the list printed in the terminal your device 
    my_dev = 0
    soundStream = SoundR(sampleRate=FsOut, deviceOut=my_dev, channelsOut=2) #<-----


    print('--> Load and play.')
    soundStream.load(s1)
    soundStream.playSound()

    time.sleep(1)

    soundStream.stopSound()

if __name__ == '__main__':
    start()
