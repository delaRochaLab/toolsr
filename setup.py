#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('toolsR/__init__.py', 'r') as fd: version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                                                                 fd.read(), re.MULTILINE).group(1)
if not version:
    raise RuntimeError('Cannot find version information')

requirements = [
    'sounddevice', 'numpy', 'scipy', 'slackclient'
]

setup(
    name='toolsR',
    version=version,
    description="""toolsR is a control system for camera USB and triggering sound""",
    author=['Rachid Azizi'],
    author_email='azi92rach@gmail.com',
    license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
    url='https://bitbucket.org/azi92rach/toolsr.git',

    include_package_data=True,
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),
    install_requires=requirements,
)
